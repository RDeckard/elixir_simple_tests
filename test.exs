defmodule Test do
  defmodule Client do
    # API
    def start_server(state) when is_integer(state) do
      GenServer.start_link(Test.Server, state)
    end

    def state(server_pid) do
      GenServer.call(server_pid, :state)
    end
    defdelegate state(server_pid, new_state), to: Test.Client, as: :set_state

    def set_state(server_pid, new_state) when is_integer(new_state) do
      GenServer.cast(server_pid, {:set_state, new_state})
    end

    def inc(server_pid, value) when is_integer(value) do
      GenServer.cast(server_pid, {:inc, value})
    end
  end

  defmodule Server do
    use GenServer

    # SERVER
    def handle_call(:state, _from, state) do
      {:reply, state, state}
    end

    def handle_cast({:set_state, new_state}, _state) do
      {:noreply, new_state}
    end

    def handle_cast({:inc, value}, state) do
      {:noreply, state + value}
    end

    def handle_info(message, state) do
      IO.puts "Received by #{inspect(self())}: \"#{message}\""
      {:noreply, state}
    end
  end

  {:ok, test_server} = Client.start_server(0)

  IO.inspect Client.state(test_server)
  IO.inspect Client.inc(test_server, 1)
  IO.inspect Client.state(test_server)
  IO.inspect Client.inc(test_server, 10)
  IO.inspect Client.state(test_server)
  IO.inspect Client.state(test_server, 2)
  IO.inspect Client.state(test_server)
  IO.inspect Client.set_state(test_server, 3)
  IO.inspect Client.state(test_server)

  send(test_server, "I'm #{inspect(self())}: salut !")
end
